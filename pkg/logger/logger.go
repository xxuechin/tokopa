package logger

import (
	"github.com/google/wire"
	"github.com/natefinch/lumberjack"
	"github.com/portgas-x/tokopa/pkg/config"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var Provider = wire.NewSet(New)
var sugarLogger *zap.SugaredLogger

func New(cfg *config.Config) (logger *zap.SugaredLogger, err error) {
	// writeSyncer := getLogWriter()
	// encoder := getEncoder()
	// _ := zapcore.NewCore(encoder, writeSyncer, zapcore.DebugLevel)

	// logger, _ := zap.New(core, zap.AddCaller())
	// sugarLogger = logger.Sugar()
	return
}

func getEncoder() zapcore.Encoder {
	encoderConfig := zap.NewProductionEncoderConfig()
	encoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder
	encoderConfig.EncodeLevel = zapcore.CapitalLevelEncoder
	return zapcore.NewConsoleEncoder(encoderConfig)
}

func getLogWriter() zapcore.WriteSyncer {
	lumberJackLogger := &lumberjack.Logger{
		Filename:   "./test.log",
		MaxSize:    1,
		MaxBackups: 5,
		MaxAge:     30,
		Compress:   false,
	}
	return zapcore.AddSync(lumberJackLogger)
}
