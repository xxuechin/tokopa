package database

import (
	"database/sql"

	"github.com/google/wire"
	"github.com/portgas-x/tokopa/pkg/config"
)

var Provider = wire.NewSet(New)

func New(cfg *config.Config) (db *sql.DB, err error) {
	db, err = sql.Open("mysql", cfg.Database.Dsn)
	if err != nil {
		return
	}
	if err = db.Ping(); err != nil {
		return
	}
	return db, nil
}
