package main

import (
	"github.com/google/wire"
	"github.com/portgas-x/tokopa/pkg/config"
	"github.com/portgas-x/tokopa/pkg/database"
)

func InitApp() (*App, error) {
	panic(wire.Build(config.Provider, database.Provider, NewApp))
}
