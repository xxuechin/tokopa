package main

import (
	"database/sql"
	"log"

	"github.com/portgas-x/tokopa/pkg/config"
)

type App struct { // 最终需要的对象
	db  *sql.DB
	cfg *config.Config
}

func NewApp(d *sql.DB, c *config.Config) *App {
	return &App{db: d, cfg: c}
}

func main() {
	// r := gin.New()
	_, err := InitApp()
	if err != nil {
		log.Fatal(err)
	}
	// var version string
	// logger, _ := zap.NewProduction()
	// r.Use(ginzap.Ginzap(logger, time.RFC3339, true))
	// r.Use(ginzap.RecoveryWithZap(logger, true))
	// r.Run()
}
